# Terraform with GitLab CI - Advanced Architecture

Ce dépôt de code contient une chaine CI/CD complète pour déployer du code Terraform avec Gitlab CI dans le cas d'une architecture avec plusieurs couches en utilisant le concept de [pipelines enfants](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html).

Dans cet exemple, le job ``generate-terraform-pipeline`` du fichier ``terraform.gitlab-ci.yml`` permet de générer le YAML de la pipeline enfant qui permettra de déployer de manière dynamique l'infrastructure.

Le code permettant de générer la pipeline enfant se trouve dans le dossier ``ci/pipelines`` :

* Le fichier de configuration ``pipeline_config.terraform.yaml`` déclare les couches Terraform contenues dans notre répertoire ;
* Le template ``pipeline_template.terraform.yaml.j2`` est un modèle pour la génération d'une pipeline enfant composé de plusieurs variables à définir ;
* Le fichier ``generate_pipeline.terraform.py`` récupère la configuration et génère dynamiquement les fichiers des pipelines enfants.

La code Terraform quant à lui se compose de deux dossiers :

* Le premier ``folders`` permet de créer des dossiers au sein de l'organisation Google Cloud et d'ajouter des restrictions ;
* Le deuxième ``projects`` permet de déployer un projet avec une couche réseau (VPC).
