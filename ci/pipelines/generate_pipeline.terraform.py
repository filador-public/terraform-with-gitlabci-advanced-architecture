#!/usr/bin/python3

"""Import Python modules"""
import logging
from os import walk
import yaml
from jinja2 import Environment, FileSystemLoader
from yaml.loader import SafeLoader

def generate_template(layer, path, tfvars_files):
    """Generate jinja2 template to create YAML files for Gitlab CI"""
    environment = Environment(loader=FileSystemLoader('./ci/pipelines'))
    template = environment.get_template('pipeline_template.terraform.yaml.j2')

    filename = layer + ".pipeline.terraform.gitlab-ci.yml"

    content = template.render(
        layer=layer,
        path=path,
        tfvars_files=tfvars_files
    )

    with open(filename, mode="w", encoding="utf-8") as message:
        message.write(content)

def get_terraform_configuration(path):
    """Get Terraform configurations for a given path"""

    configuration_path = path + '/configurations'
    tfvars_files = []

    # Retrieve all Terraform configuration files
    for (dirpath, dirnames, filenames) in walk(configuration_path):
        tfvars_files = filenames
        break

    return tfvars_files

def main():
    """Main"""
    logging.basicConfig(
        filename='generate_pipeline.terraform.log',
        format='%(asctime)s %(name)s:%(levelname)s:%(message)s',
        level=logging.DEBUG
    )

    # Open pipeline configuration file
    with open("./ci/pipelines/pipeline_config.terraform.yaml", 'r', encoding="utf-8") as file:
        try:
            yaml_content = yaml.load(file, Loader=SafeLoader)

            logging.debug('Layers : %s', yaml_content['layers'])

            for layer in yaml_content['layers']:
                logging.debug('Current layer : %s', layer)

                # Get path
                path = yaml_content['layers'][layer]['path']

                # Retrieve Terraform configurations
                tfvars_files = get_terraform_configuration(path)

                # Generate pipeline template
                generate_template(
                    layer=layer,
                    path=path,
                    tfvars_files=tfvars_files
                )

        # Syntax error with YAML file
        except yaml.YAMLError as error:
            logging.debug("Parse error :")
            logging.debug(error)

if __name__ == "__main__":
    main()
