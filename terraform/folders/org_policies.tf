resource "google_folder_organization_policy" "serial_port_policy" {
  folder     = google_folder.folder.id
  constraint = "compute.disableSerialPortAccess"

  boolean_policy {
    enforced = true
  }
}