variable "org_domain_name" {
  type        = string
  description = "The organisation domain name"
}

variable "folder_name" {
  type        = string
  description = "The folder name"
}
