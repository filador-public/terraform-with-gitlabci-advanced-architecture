data "google_organization" "organization" {
  domain = var.org_domain_name
}

resource "google_folder" "folder" {
  display_name = var.folder_name
  parent       = "organizations/${data.google_organization.organization.org_id}"
}
