folder_id    = ""
project_name = "external-services"

region = "us-east1"

network_name = "vpc-external-services"
subnet_name  = "sne-external-services"
subnet_cidr  = "10.0.1.0/24"

billing_amount = 1
