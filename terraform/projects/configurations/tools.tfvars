folder_id    = ""
project_name = "tools"

region = "us-east1"

network_name = "vpc-tools"
subnet_name  = "sne-tools"
subnet_cidr  = "10.0.3.0/24"

billing_amount = 1
