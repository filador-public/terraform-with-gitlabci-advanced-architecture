folder_id    = ""
project_name = "backend"

region = "us-east1"

network_name = "vpc-backend"
subnet_name  = "sne-backend"
subnet_cidr  = "10.0.0.0/24"

billing_amount = 1
