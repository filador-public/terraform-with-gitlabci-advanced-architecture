output "project_id" {
  value       = module.external_services.project_id
  description = "The project ID"
}
