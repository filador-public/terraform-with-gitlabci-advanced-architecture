variable "org_domain_name" {
  type        = string
  description = "The organisation domain name"
}

variable "folder_id" {
  type        = number
  description = "The folder ID"
}

variable "project_name" {
  type        = string
  description = "The project name"
}

variable "billing_account_id" {
  type        = string
  description = "The billing account ID"
}

variable "billing_amount" {
  type        = string
  description = "The billing amount threshold"
}

variable "region" {
  type        = string
  description = "The Google Cloud region"
}

variable "network_name" {
  type        = string
  description = "The VPC network name"
}

variable "subnet_name" {
  type        = string
  description = "The subnetwork name"
}

variable "subnet_cidr" {
  type        = string
  description = "The subnetwork cidr range"
}
